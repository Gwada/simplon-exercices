<?php

const OK = "OK\n";
const ERROR = "ERROR\n";
const DIR = 'private';
const FILENAME = 'private/passwd';
$log = $_POST['login'] ?? null;
$pass = $_POST['passwd'] ?? null;
$submit = $_POST['submit'] ?? null;

if (!$log || !$pass || $submit != "OK" ) {
	echo ERROR;
    return;
}

if (!file_exists(DIR)) {
	mkdir(DIR, 0777, true);
}

$account = ['login'=> $log, 'value' => password_hash($pass, PASSWORD_DEFAULT)];

if (file_exists(FILENAME)) {
	$content = file_get_contents(FILENAME);
	$accounts = unserialize($content);
	foreach($accounts as $value) {
		if ($value['login'] == $log) {
			echo ERROR;
			return;
		}
	}
	$accounts[] = $account;
	push_data(FILENAME, $accounts);
} else {
	$accounts[] = $account;
	push_data(FILENAME, $accounts);
}

function push_data (string $filename, array $accounts)
{	
	$serialized = serialize($accounts);
	file_put_contents($filename, $serialized);
	echo OK;
}

// composer test-locally -- tests/phpLesBases/ex21/createTest.php


// CODE DE DIMITRI

// const DIR = 'private';
// const FILE_DIR = 'private/passwd';

// const ERROR_OUTPUT = "ERROR\n";
// const SUCCES_OUTPUT = "OK\n";

// const PASSWORD_INPUT_NAME = 'passwd';
// const SUBMIT_INPUT_NAME = 'submit';
// const SUBMIT_INPUT_VALUE = 'OK';
// const USERNAME_INPUT_NAME = 'login';

// if (
//     ($_POST[SUBMIT_INPUT_NAME] ?? null) !== SUBMIT_INPUT_VALUE
//     || empty(($_POST[USERNAME_INPUT_NAME] ?? null))
//     || empty(($_POST[PASSWORD_INPUT_NAME] ?? null))
// ) {
//     echo ERROR_OUTPUT;
//     exit;
// }

// if (!file_exists(DIR)) {
//     mkdir(DIR, 0755, true);
// }

// if (!file_exists(FILE_DIR)) {
//     file_put_contents(FILE_DIR, '');
// }

// $login = $_POST[USERNAME_INPUT_NAME];
// $credentialDatabase = unserialize(file_get_contents(FILE_DIR)) ?: [];

// foreach ($credentialDatabase as [USERNAME_INPUT_NAME => $userLogin]) {
//     if ($userLogin === $login) {
//         echo ERROR_OUTPUT;
//         exit;
//     }
// }

// $credentialDatabase[] = [USERNAME_INPUT_NAME => $login, PASSWORD_INPUT_NAME => password_hash($_POST[PASSWORD_INPUT_NAME], PASSWORD_DEFAULT)];
// file_put_contents(FILE_DIR, serialize($credentialDatabase));

// echo SUCCES_OUTPUT;