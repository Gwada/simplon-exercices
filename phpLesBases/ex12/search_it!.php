<?php

$the_exploded = [];
$params = [];
$magic_key = $argv[1];

foreach (array_slice($argv, 2) as $value) {
    if (preg_match('/.+:.+/', $value, $match)) {
        $the_exploded = explode(':', $value);
        $params[$the_exploded[0]] = $the_exploded[1];
    }
}

if (isset($params[$magic_key])) {
    echo $params[$magic_key] . "\n";
}
