<?php

if ($argc != 2) {
    echo "Incorrect Parameters\n";
    exit;
}

// Déclaration des variables utilisées.

// Déclaration des patterns Regex.
$super_regex = "[(-?\d*\.{0,1}\d+)([\*|\+|\-|\/|\%])((?1))]";
$alpha = '/[A-Za-z]+/';

// On supprime les espaces.
$epur = preg_replace("/\s+/", '', trim($argv[1]));

if (preg_match($super_regex, $epur, $match) && !preg_match($alpha, $epur, $match_alpha)) {
    $number1 = +$match[1];
    $number2 = +$match[3];
    $op = $match[2];

    switch ($op) {
    case '+':
      echo $number1 + $number2 . "\n";
      break;
    case '-':
      echo $number1 - $number2 . "\n";
      break;
    case '*':
      echo $number1 * $number2 . "\n";
      break;
    case '/':
      if (($number1 || $number2) == 0) {
          echo '0';
      } else {
          echo $number1 / $number2 . "\n";
      }
      break;
    case '%':
      if (!$number1 || !$number2) {
          echo 0 . "\n";
      } else {
          echo abs(fmod($number1, $number2)) . "\n";
      }
      break;
    default:
      echo "Incorrect Parameters\n";
      break;
  }
} else {
    echo "Syntax Error\n";
}
